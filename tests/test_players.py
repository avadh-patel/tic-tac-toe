
import unittest
import sys

from io import StringIO
from tictactoe.players import Human, Computer, newPlayer
from tictactoe import _Engine
from .test_interface import MockCLI


class TestPlayers(unittest.TestCase):

    def test_gen_new_player(self):
        # First create a new human
        human = newPlayer("human", "x")
        self.assertIsNotNone(human)
        self.assertEqual(human.sign, "x")

        # Now get computer
        computer = newPlayer("computer", "o")
        self.assertIsNotNone(computer)
        self.assertEqual(computer.sign, "o")

        with self.assertRaises(ValueError):
            newPlayer("invalid-name", "s")


class TestHuman(unittest.TestCase):

    def test_new_player(self):
        human = Human("o")
        self.assertIsNotNone(human)
        self.assertEqual(human.name, "You")
        self.assertEqual(human.sign, "o")

    def test_next_move(self):
        human = Human("o")
        cli = MockCLI()
        eng = _Engine(human, human, )
        eng._interface = cli
        eng._startGame()

        # Check move tests
        for i in range(1, 4):
            v = human.checkMove("%s" % i, "x")
            self.assertEqual(v, i)

        # Check invalid inputs
        v = human.checkMove(" ", "x")
        self.assertEqual(v, False)

        # Check invalid inputs
        v = human.checkMove("abc", "x")
        self.assertEqual(v, False)

        for i in [("1 1", 1, 1), ("2 1", 2, 1)]:
            cli.mInp.truncate(0)
            cli.mInp.seek(0)
            cli.mInp.write("%s\n" % i[0])
            cli.mInp.seek(0)
            x, y = human.nextMove()
            self.assertEqual(x, i[1] - 1)
            self.assertEqual(y, i[2] - 1)


class TestComputer(unittest.TestCase):

    def test_new_player(self):
        computer = Computer("x")
        self.assertIsNotNone(computer)
        self.assertEqual(computer.name, "Computer")
        self.assertEqual(computer.sign, "x")

    def test_next_move(self):
        computer = Computer("x")

        # Generate random move and check they are valid
        for i in range(10):
            move = computer.nextMove()
            self.assertGreaterEqual(move[0], 0)
            self.assertGreaterEqual(move[1], 0)
            self.assertLess(move[0], 3)
            self.assertLess(move[1], 3)

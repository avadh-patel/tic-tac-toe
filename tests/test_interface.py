
import unittest
import sys

from io import StringIO
from tictactoe.interface import newInterface, CLIInterface


class MockCLI(CLIInterface):
    def __init__(self):
        self.mOut = StringIO()
        self.mErr = StringIO()
        self.mInp = StringIO()
        super(MockCLI, self).__init__(inp=self.mInp,
                                      out=self.mOut,
                                      err=self.mErr)


class MockUser:
    def __init__(self, symbol):
        self.s = symbol

    @property
    def sign(self):
        return self.s


class TestInterface(unittest.TestCase):

    def test_new_interface(self):
        cli = newInterface("cli")
        self.assertIsNotNone(cli)
        self.assertEqual(cli.name, "cli")

        with self.assertRaises(ValueError):
            newInterface("invalid-interface")


class TestCliInterface(unittest.TestCase):

    def test_cli_interface(self):
        cli = CLIInterface()
        self.assertIsNotNone(cli)
        self.assertEqual(cli.out, sys.stdout)
        self.assertEqual(cli.err, sys.stderr)

    def test_cli_customout(self):
        cli = MockCLI()

        # Write a msg check on out
        msg = "Hello World!"
        cli.showMsg(msg)
        output = cli.mOut.getvalue().strip()
        self.assertEqual(output, msg)

        msg = "Get User Input: "
        inpMsg = "testing input"
        cli.mInp.truncate(0)
        cli.mInp.write("%s\n" % inpMsg)
        cli.mInp.seek(0)
        inpRecvd = cli.getUserInput(msg)
        self.assertEqual(inpRecvd, inpMsg)

        errMsg = "Error!"
        cli.showError(errMsg)
        self.assertEqual(cli.mErr.getvalue().strip(), errMsg)

    def test_cli_grid(self):
        cli = MockCLI()
        grid = [[None for i in range(3)] for j in range(3)]

        p1 = MockUser("x")
        p2 = MockUser("o")

        grid[0][0] = p1
        grid[1][1] = p2
        grid[2][2] = p1

        cli.displayGrid(grid)
        gridOut = cli.mOut.getvalue()
        expGrid = """ x   1   2   3
y  -------------
1  | x |   |   |
   -------------
2  |   | o |   |
   -------------
3  |   |   | x |
   -------------
"""
        self.assertEqual(gridOut, expGrid)

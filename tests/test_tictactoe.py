
import unittest

import tictactoe


class TestTicTacToe(unittest.TestCase):

    def test_tictactoe_newGame(self):
        engine = tictactoe.newGame(None, None)
        self.assertIsNotNone(engine)

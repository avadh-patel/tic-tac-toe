
import unittest

from tictactoe import _Engine
from tictactoe.players import Human, Computer
from .test_interface import MockCLI


class TestEngine(unittest.TestCase):

    def test_default_engine(self):
        engine = _Engine(None, None)
        self.assertIsNotNone(engine)
        self.assertIsNotNone(engine.grid)
        self.assertIsNotNone(engine.players)
        self.assertIsNotNone(engine.interface)

        # check grid to have all cells None
        for i in range(3):
            for j in range(3):
                self.assertIsNone(engine.grid[i][j])

        # check default interface to be cli
        self.assertEqual(engine.interface.name, "cli")

    def test_new_engine_err(self):
        with self.assertRaises(ValueError):
            _Engine(None, None, interface="web")

    def test_winner(self):
        p1 = {}
        p2 = {}
        engine = _Engine(p1, p2)

        # Make sure no winner when board is empty
        for i in range(3):
            for j in range(3):
                self.assertFalse(engine._winner(p1, i, j))
                self.assertFalse(engine._winner(p2, i, j))

        for j in range(3):
            engine = _Engine(p1, p2)
            for i in [[j, 0, None], [j, 1, None], [j, 2, p1]]:
                engine.grid[i[0]][i[1]] = p1
                self.assertEqual(engine._winner(p1, i[0], i[1]), i[2])

        for j in range(3):
            # Clear the engine!
            engine = _Engine(p1, p2)
            for i in [[0, j, None], [1, j, None], [2, j, p1]]:
                engine.grid[i[0]][i[1]] = p1
                self.assertEqual(engine._winner(p1, i[0], i[1]), i[2])

        # Clear the engine!
        engine = _Engine(p1, p2)
        for i in [[0, 0, None], [1, 1, None], [2, 2, p1]]:
            engine.grid[i[0]][i[1]] = p1
            self.assertEqual(engine._winner(p1, i[0], i[1]), i[2])

        # Clear the engine!
        engine = _Engine(p1, p2)
        for i in [[0, 2, None], [1, 1, None], [2, 0, p1]]:
            engine.grid[i[0]][i[1]] = p1
            self.assertEqual(engine._winner(p1, i[0], i[1]), i[2])

    def test_draw(self):
        p1 = {}
        p2 = {}
        engine = _Engine(p1, p2)

        self.assertFalse(engine._draw())

        for i in range(3):
            for j in range(3):
                engine.grid[i][j] = p1
        self.assertTrue(engine._draw())

    def test_valid_move(self):
        engine = _Engine({}, {})
        self.assertTrue(engine._validMove([0, 0]))
        self.assertTrue(engine._validMove([0, 1]))
        self.assertTrue(engine._validMove([1, 1]))

        engine.grid[0][0] = {}
        engine.grid[0][1] = {}
        engine.grid[1][1] = {}
        self.assertFalse(engine._validMove([0, 0]))
        self.assertFalse(engine._validMove([0, 1]))
        self.assertFalse(engine._validMove([1, 1]))

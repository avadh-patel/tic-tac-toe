#!/usr/bin/env python3

import tictactoe


def main():

    # Create players
    user = tictactoe.newPlayer("human", "x")
    computer = tictactoe.newPlayer("computer", "o")

    print("Lets play Tic Tac Toe.")
    print("Your cells are marked as 'x', and you will enter your selection as 'x y' where x and y can be 1, 2 or 3.")

    while True:
        engine = tictactoe.newGame(user, computer)
        engine.play()

        while True:
            inp = input("Do you want to play again? (Y/n): ")
            inp = inp.strip().lower()

            if inp == 'n':
                print("Good bye!")
                return
            elif inp == 'y' or inp == '':
                print("Sure, lets start a new game!")
                break

            print("Invalid input!")


if __name__ == "__main__":
    main()

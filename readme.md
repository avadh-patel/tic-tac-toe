# Tic Tac Toe

A simple Tic Tac Toe game to play in CLI.

## Requirements
python >= 3.5

## How to play
To play the game, run 'playGame.py' script as below:

    $ ./playGame.py

Enjoy!

## To run tests

	$ python3 -m unittest


import sys

from .baseInterface import Interface


class CLIInterface(Interface):

    def __init__(self, **kwargs):
        super(CLIInterface, self).__init__("cli")

        # Initalize in, out and err objs
        if "inp" in kwargs:
            self.inp = kwargs["inp"]
        else:
            self.inp = sys.stdin

        if "out" in kwargs:
            self.out = kwargs["out"]
        else:
            self.out = sys.stdout

        if "err" in kwargs:
            self.err = kwargs["err"]
        else:
            self.err = sys.stderr

    def displayGrid(self, grid):
        # Dispaly grid of tic-tac-toe with x and y labels
        msg = " x   1   2   3\n"
        msg += "y  -------------\n"

        for i in range(3):
            msg += "%s  |" % (i + 1)
            for j in range(3):
                if grid[i][j]:
                    msg += " %s |" % (grid[i][j].sign)
                else:
                    msg += "   |"
            msg += "\n   -------------\n"

        self.out.write(msg)

    def getUserInput(self, msg):
        # Loop until a non-empty msg is received
        while True:
            self.out.write(msg)
            self.out.flush()
            userInp = self.inp.readline().strip()
            if len(userInp) > 0:
                return userInp

    def showMsg(self, msg):
        msg += "\n"
        self.out.write(msg)

    def showError(self, errMsg):
        errMsg += "\n"
        self.err.write(errMsg)


from .cliInterface import CLIInterface


def newInterface(name):
    if name is "cli":
        return CLIInterface()
    else:
        raise ValueError("Invalid Interface name: %s" % name)

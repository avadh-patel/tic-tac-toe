
from abc import ABC, abstractmethod


class Interface(ABC):

    def __init__(self, name):
        self._name = name

    @property
    def name(self):
        return self._name

    @abstractmethod
    def displayGrid(self, grid):
        pass

    @abstractmethod
    def getUserInput(self, msg):
        pass

    @abstractmethod
    def showMsg(self, msg):
        pass

    @abstractmethod
    def showError(self, errMsg):
        pass

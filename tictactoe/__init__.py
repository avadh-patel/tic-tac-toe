
from .engine import _Engine
from .players import newPlayer


def newGame(player1, player2, **kwargs):
    engine = _Engine(player1, player2, **kwargs)
    return engine

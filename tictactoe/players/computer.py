
from random import randint
from .basePlayer import Player


# Computer Player Class
class Computer(Player):

    def __init__(self, sign):
        super(Computer, self).__init__("Computer", sign)

    def nextMove(self):
        # Random move generator
        x = randint(0, 2)
        y = randint(0, 2)
        return (x, y)

    def invalidMove(self, msg):
        pass

    def won(self):
        pass

    def lost(self):
        pass

    def draw(self):
        pass

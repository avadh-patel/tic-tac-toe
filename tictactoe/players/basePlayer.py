
from abc import ABC, abstractmethod


# Abstract Player Class
class Player(ABC):

    def __init__(self, name, sign):
        self._name = name
        self._sign = sign
        self.game = None

    def startGame(self, game):
        self.game = game

    def endGame(self):
        self.game = None

    @property
    def name(self):
        return self._name

    @property
    def sign(self):
        return self._sign

    @property
    def interface(self):
        if not self.game:
            raise ValueError("Can't get interface without game")
        return self.game.interface

    @abstractmethod
    def nextMove(self):
        pass

    @abstractmethod
    def invalidMove(self, msg):
        pass

    @abstractmethod
    def won(self):
        pass

    @abstractmethod
    def lost(self):
        pass

    @abstractmethod
    def draw(self):
        pass

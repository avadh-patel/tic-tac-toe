
from .human import Human
from .computer import Computer


def newPlayer(playerName, symbol):
    if playerName is "human":
        return Human(symbol)
    elif playerName is "computer":
        return Computer(symbol)
    else:
        raise ValueError("Invalid player name: %s" % playerName)

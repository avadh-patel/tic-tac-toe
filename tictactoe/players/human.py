
from .basePlayer import Player


# Human Player Class
class Human(Player):

    def __init__(self, sign):
        super(Human, self).__init__("You", sign)

    def checkMove(self, v, dim):
        if not v or len(v.strip()) == 0:
            self.interface.showError("Your %s value is not valid." % (dim))
            return False
        try:
            v = int(v)
            if v < 1 or v > 3:
                self.interface.showError("Your %s value is not between 1 and 3" % dim)
                return False
            return v
        except ValueError as e:
            self.interface.showError("Your %s value is not valid. Error: %s" % (dim, str(e)))
            return False

    def nextMove(self):
        # Get user's input until its a valid input between [1..3] [1..3]
        # Player does not check if there is conflict on the input or not,
        # the game engine will check the conflict and ask for new input.
        while True:
            move = self.interface.getUserInput("Enter Your Move: ")

            try:
                x, y = move.split()
                x = self.checkMove(x, "X")
                if x is False:
                    continue
                y = self.checkMove(y, "Y")
                if y is False:
                    continue

                # Valid input! - Conflict will be checked by game
                return (x - 1, y - 1)
            except ValueError as e:
                self.interface.showError("Invalid input, please specify your selection as 'x y' where x and y are between 1 and 3")

    def invalidMove(self, msg):
        self.interface.showError(msg)

    def won(self):
        self.interface.showMsg("Congratulations You Won!")

    def lost(self):
        self.interface.showMsg("Oops You Lost!")

    def draw(self):
        self.interface.showMsg("Draw Game!")

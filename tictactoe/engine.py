
from .interface import newInterface


# Tic Tac Toe Game Engine
class _Engine:

    def __init__(self, player1, player2, **kwargs):
        self.players = [player1, player2]
        self.grid = [[None for i in range(3)] for j in range(3)]
        self.nextPlayerIdx = 0

        if "interface" not in kwargs:
            kwargs["interface"] = "cli"

        self._interface = newInterface(kwargs["interface"])

    def _startGame(self):
        for p in self.players:
            p.startGame(self)

    def _endGame(self):
        for p in self.players:
            p.endGame()

    def _winner(self, player, x, y):
        # Check if player is a winner from position x and y
        hWinner = True  # Horizonal Direction
        vWinner = True  # Vertical Direction
        dWinner = True  # Diagonal Direction 1
        dWinner2 = True  # Diagonal Direction 2

        for i in range(3):
            hWinner &= self.grid[x-i][y] == player
            vWinner &= self.grid[x][y-i] == player
            dWinner &= self.grid[i][i] == player
            dWinner2 &= self.grid[2-i][i] == player

        if hWinner or vWinner or dWinner or dWinner2:
            return player

        return None

    def _validMove(self, move):
        # Only check if requested cell is free or not
        # Player object check for boundry and other invalid moves
        if len(move) != 2:
            return False

        if self.grid[move[0]][move[1]] is None:
            return True

        return False

    def _draw(self):
        empty = 0
        for i in range(3):
            for j in range(3):
                if self.grid[i][j] is None:
                    empty += 1

        if empty == 0:
            return True

        return False

    def play(self):
        self._startGame()
        winner = None

        while winner is None and self._draw() is False:
            player = self.players[self.nextPlayerIdx]
            self.interface.displayGrid(self.grid)

            # Get a valid move from current player
            move = None
            while move is None:
                move = player.nextMove()
                if self._validMove(move):
                    break
                player.invalidMove("Selected location is not valid.")
                move = None

            self.grid[move[0]][move[1]] = player
            self.interface.showMsg("%s selected: %d %d" % (
                player.name, move[0] + 1, move[1] + 1))

            winner = self._winner(player, move[0], move[1])
            self.nextPlayerIdx += 1
            self.nextPlayerIdx %= len(self.players)

        self.interface.showMsg("Game Ended!")
        self.interface.displayGrid(self.grid)

        for player in self.players:
            if winner is None:
                player.draw()
            elif player == winner:
                player.won()
            else:
                player.lost()

        self._endGame()

    @property
    def interface(self):
        return self._interface

    def __repr__(self):
        ret = "Engine: [p1: %s] [p2: %s]\n" % (
            self.players[0], self.players[1])
        ret += "Grid:\n"
        for i in range(3):
            for j in range(3):
                cell = self.grid[(i * 3) + j]
                if cell:
                    ret += "%s" % (cell)
                else:
                    ret += " "
                if j < 2:
                    ret += " "
            ret += "\n"
        return ret
